--[ GIBBON JOYEUX ]--
--[    Sunlight   ]--
-- set_layer.lua

--[[
--
-- Set ascii active layer
--
--]]

local function	set_layer(	self,
							layer )
	self.layer = layer or self.canvas
	self.layer_height = #self.layer
	self.layer_width = #self.layer[ 1 ]
end

return set_layer

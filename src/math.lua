--[ GIBBON JOYEUX ]--
-- math.lua

--[[
--
-- Math utils
--
--]]

floor 			= math.floor
ceil			= math.ceil
abs				= math.abs
max				= math.max
min				= math.min
sqrt			= math.sqrt
cos				= math.cos
sin				= math.sin
PI				= math.pi
TWO_PI			= math.pi * 2

function		round( x )
	return floor( x + 0.5 )
end

function		sign( x )
	return ( x < 0 ) and -1 or ( ( x > 0 ) and 1 or 0 )
end

function		constrain( x, min_x, max_x )
	return max( min_x, min( max_x, x ) )
end

function		abs_max(	value,	-- value to submit --
							limit )	-- absolute limit --
	--[[
	--
	-- Contrains the passed value into the limit as absolute value
	--
	--]]

	if value < 0 then
		return max( value, -limit )
	end
	return min( value, limit )
end

function		map(	x,
						from_0,
						to_0,
						from_1,
						to_1,
						bounded )
	--[[
	--
	-- Remap x from passed boundary to another
	--
	--]]

	x = from_1 + ( ( x - from_0 ) / ( to_0 - from_0 ) ) * ( to_1 - from_1 )
	if bounded == true then
		x = min( to_1, max( from_1, x ) )
	end
	return x
end

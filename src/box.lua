--[ GIBBON JOYEUX ]--
--[    Sunlight   ]--
-- box.lua

--[[
--
-- Draw a box
--
--]]

--------------------------------------------------------------------------------
 ------------------------------ LOCAL FUNCTIONS -------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
 ------------------------------ PUBLIC FUNCTIONS ------------------------------
--------------------------------------------------------------------------------

local function		box(	self,
							x,				-- box x coordinate --
							y,				-- box y coordinate --
							width,			-- box width --
							height,			-- box height --
							char_outline,	-- box outline character --
							char_fill )		-- box fill character --
	local			pos_x, pos_y
	local			cell_x, cell_y
	local			char

	-- for each line --
	for pos_y = 0, height - 1 do
		cell_y = y + pos_y
		if cell_y > self.layer_height then
			break
		end
		-- for each column --
		for pos_x = 0, width - 1 do
			cell_x = x + pos_x
			if cell_x > self.layer_width then
				break
			end
			-- draw character --
			if cell_x > 1 then
				if pos_y == 0 or pos_y == height - 1
				or pos_x == 0 or pos_x == width - 1 then
					self.layer[ cell_y ][ cell_x ] = char_outline
				else
					self.layer[ cell_y ][ cell_x ] = char_fill
				end
			end
		end
	end
end

return box

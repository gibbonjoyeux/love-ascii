--[ GIBBON JOYEUX ]--
--[    Sunlight   ]--
-- text.lua

--[[
--
-- Draw text to layer
--
--]]

--------------------------------------------------------------------------------
 ------------------------------ LOCAL FUNCTIONS -------------------------------
--------------------------------------------------------------------------------

local function	split_into_words( s )
	--[[
	--
	-- Split text into words
	--
	--]]
    local res
	
	res = {}
    for w in s:gmatch( "%S+" ) do
        res[ #res + 1 ] = w
    end
    return res
end

local function	wrap_text(	text,
							line_width )
	--[[
	--
	-- Return a wrapped text table from text and line width
	--
	--]]
    local	spaceleft
    local	lines
    local	line

	spaceleft = line_width
	lines = {}
	line = {}
    for _, word in ipairs( split_into_words( text ) ) do
		-- not enough space --
		if #word + 1 > spaceleft then
			lines[ #lines + 1 ] = table.concat( line, ' ' )
			line = { word }
			spaceleft = line_width - #word
		-- enough space --
		else
			line[ #line + 1 ] = word
			-- 1st word of all text --
			if #line == 1 then
				spaceleft = line_width - #word
			-- else --
			else
				spaceleft = spaceleft - ( #word + 1 )
			end
		end
    end
	lines[ #lines + 1 ] = table.concat( line, ' ' )
    return lines
end

local function	put_text(	self,
							lines,
							x,
							y )
	local		line
	local		pos_x
	local		i

	for _, line in ipairs( lines ) do
		if y > 0 and y <= self.layer_height then
			-- put line --
			for i = 1, #line do
				pos_x = x + i - 1
				-- out on left
				if pos_x < 1 then
					-- wait until next iteration
					i = 1 - x
				-- out on right
				elseif pos_x > self.layer_width then
					break
				-- well positionned
				else
					self.layer[ y ][ pos_x ] = line:sub( i, i )
				end
			end
		end
		y = y + 1
		if y > self.layer_height then
			break
		end
	end
end

--------------------------------------------------------------------------------
 ------------------------------ PUBLIC FUNCTIONS ------------------------------
--------------------------------------------------------------------------------

local function	text(	self,
						text,			-- text to write --
						x,				-- x coordinate of the text --
						y,				-- y coordinate of the text --
						line_width )	-- line width of the text --
	local		wrapped_text

	-------------------------
	-- check and wrap text --
	-------------------------
	if x > self.layer_width or x + line_width <= 1 or y > self.layer_height then
		return
	end
	wrapped_text = wrap_text( text, line_width )
	if y + #wrapped_text <= 1 then
		return
	end
	--------------
	-- put text --
	--------------
	put_text( self, wrapped_text, x, y )
end

return text

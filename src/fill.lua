--[ GIBBON JOYEUX ]--
--[    Sunlight   ]--
-- polygon.lua

--[[
--
-- Draw a polygon
--
--]]

--------------------------------------------------------------------------------
 ------------------------------ LOCAL FUNCTIONS -------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
 ------------------------------ PUBLIC FUNCTIONS ------------------------------
--------------------------------------------------------------------------------

local function	fill(	self,
						x,
						y,
						char )
	local		to_change

	to_change = self.layer[ y ][ x ]
	if to_change == char then
		return
	end
	self.layer[ y ][ x ] = char
	if x > 1 and self.layer[ y ][ x - 1 ] == to_change then
		self:fill( x - 1, y, char )
	end
	if x < self.layer_width and self.layer[ y ][ x + 1 ] == to_change then
		self:fill( x + 1, y, char )
	end
	if y > 1 and self.layer[ y - 1 ][ x ] == to_change then
		self:fill( x, y - 1, char )
	end
	if y < self.layer_height and self.layer[ y + 1 ][ x ] == to_change then
		self:fill( x, y + 1, char )
	end
end

return fill

--[ GIBBON JOYEUX ]--
--[    Sunlight   ]--
-- create_layer.lua

--[[
--
-- Create an ascii layer
--
--]]

--------------------------------------------------------------------------------
 ------------------------------ LOCAL FUNCTIONS -------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
 ------------------------------ PUBLIC FUNCTIONS ------------------------------
--------------------------------------------------------------------------------

function		create_layer(	self,
								width,
								height )
	local		x, y
	local		layer
	local		line

	if not width then
		width = ascii.width
	end
	if not height then
		height = ascii.height
	end
	layer = {}
	for y = 1, height do
		line = {}
		for x = 1, width do
			line[ x ] = ' '
		end
		layer[ y ] = line
	end
	return layer
end

--------------
 -- return --
--------------
return create_layer

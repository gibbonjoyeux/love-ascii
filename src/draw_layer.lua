--[ GIBBON JOYEUX ]--
--[    Sunlight   ]--
-- draw_layer.lua

--[[
--
-- Draw passed layer to active one
--
--]]

local function	draw_layer(	self,
							layer,
							x,
							y )
	local		pos_x, pos_y
	local		final_x, final_y
	local		layer_width, layer_height

	if not x then x = 1 end
	if not y then y = 1 end
	layer_width = #layer[ 1 ]
	layer_height = #layer
	for pos_y = 1, layer_height do
		final_y = pos_y + y - 1
		if final_y > 0 and final_y <= self.layer_height then
			for pos_x = 1, layer_width do
				final_x = pos_x + x - 1
				if final_x > 0 and final_x <= self.layer_width then
					if layer[ pos_y ][ pos_x ] ~= ' ' then
						self.layer[ final_y ][ final_x ] = layer[ pos_y ][ pos_x ]
					end
				end
			end
		end
	end
end

return draw_layer

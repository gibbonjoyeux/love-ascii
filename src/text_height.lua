--[ GIBBON JOYEUX ]--
--[    Sunlight   ]--
-- text_height.lua

--[[
--
-- Give passed text its height if it would be wrapped with the given width
--
--]]

--------------------------------------------------------------------------------
 ------------------------------ LOCAL FUNCTIONS -------------------------------
--------------------------------------------------------------------------------

local function	split_into_words( s )
	--[[
	--
	-- Split text into words
	--
	--]]
    local res
	
	res = {}
    for w in s:gmatch( "%S+" ) do
        res[ #res + 1 ] = w
    end
    return res
end

local function	wrap_text(	text,
							line_width )
	--[[
	--
	-- Return a wrapped text table from text and line width
	--
	--]]
    local	spaceleft
    local	lines
    local	line

	spaceleft = line_width
	lines = {}
	line = {}
    for _, word in ipairs( split_into_words( text ) ) do
		-- not enough space --
		if #word + 1 > spaceleft then
			lines[ #lines + 1 ] = table.concat( line, ' ' )
			line = { word }
			spaceleft = line_width - #word
		-- enough space --
		else
			line[ #line + 1 ] = word
			-- 1st word of all text --
			if #line == 1 then
				spaceleft = line_width - #word
			-- else --
			else
				spaceleft = spaceleft - ( #word + 1 )
			end
		end
    end
	lines[ #lines + 1 ] = table.concat( line, ' ' )
    return lines
end

--------------------------------------------------------------------------------
 ------------------------------ PUBLIC FUNCTIONS ------------------------------
--------------------------------------------------------------------------------

local function	text_height(	self,
								text,			-- text to write --
								line_width )	-- line width of the text --

	wrapped_text = wrap_text( text, line_width )
	return #wrapped_text
end

return text_height

--[ GIBBON JOYEUX ]--
--[    Sunlight   ]--
-- polygon.lua

--[[
--
-- Draw a polygon
--
--]]

--------------------------------------------------------------------------------
 ------------------------------ LOCAL FUNCTIONS -------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
 ------------------------------ PUBLIC FUNCTIONS ------------------------------
--------------------------------------------------------------------------------

local function	polygon(	self,
							cx,			-- polygon center x coordinate --
							cy,			-- polygon center y coordinate --
							rw,			-- height of the polygon --
							rh,			-- width of the polygon --
							vertices,	-- number of vertices of the polygon --
							char,		-- character to draw polygon edge --
							fill_char,	-- character to fill polygon --
							offset )	-- offset angle from 0 to 1 --
	local		layer_src, layer_dst
	local		angle
	local		step
	local		last_x, last_y
	local		x, y
	local		i

	-- prepare filling --
	if fill_char and vertices > 2 then
		layer_src = self.layer
		layer_dst = self:create_layer( self.layer_width, self.layer_height )
		self:set_layer( layer_dst )
		self:clear()
	end
	-- offset --
	if offset == nil then
		offset = 0
	end
	step = TWO_PI / vertices
	-- prepare 1st line --
	angle = step * ( vertices - 1 ) + offset * step
	last_x = round( cx + cos( angle ) * rw )
	last_y = round( cy + sin( angle ) * rh )
	-- loop through 2 pi --
	for i = 0, TWO_PI, step do
		-- get coords --
		angle = i + offset * step
		x = round( cx + cos( angle ) * rw )
		y = round( cy + sin( angle ) * rh )
		-- print line --
		self:line( x, y, last_x, last_y, char )
		last_x = x
		last_y = y
	end
	-- fill polygon --
	if layer_src then
		self:fill( cx, cy, fill_char )
		self:set_layer( layer_src )
		self:draw_layer( layer_dst )
	end
end

return polygon

--[ GIBBON JOYEUX ]--
--[	Sunlight   ]--
-- line.lua

--[[
--
-- Draw a line
--
--]]

--------------------------------------------------------------------------------
 ------------------------------ LOCAL FUNCTIONS -------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
 ------------------------------ PUBLIC FUNCTIONS ------------------------------
--------------------------------------------------------------------------------

function		line(	self,
						x0,		-- 1st point x --
						y0,		-- 1st point y --
						x1,		-- 2nd point x --
						y1,		-- 2nd point y --
						char )	-- character to draw the line --
	local		dx, dy
	local		sx, sy
	local		err

	dx =  abs( x1 - x0 );
	sx = ( x0 < x1 ) and 1 or -1;
	dy = -abs( y1 - y0 );
	sy = ( y0 < y1 ) and 1 or -1;
	err = dx + dy;
	while true do
		if x0 >= 1 and x0 <= self.layer_width and y0 >= 1
		--[[--]] and y0 <= self.layer_height then
			self.layer[ y0 ][ x0 ] = char
		end
		if x0 == x1 and y0 == y1 then
			return
		end
		e2 = 2 * err;
		if e2 >= dy then
			err = err + dy;
			x0 = x0 + sx;
		end
		if e2 <= dx then
			err = err + dx;
			y0 = y0 + sy;
		end
	end
end

--------------
 -- return --
--------------
return line

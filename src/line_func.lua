--[ GIBBON JOYEUX ]--
--[	Sunlight   ]--
-- line_func.lua

--[[
--
-- Call the passed function for each line point
--
--]]

--------------------------------------------------------------------------------
 ------------------------------ LOCAL FUNCTIONS -------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
 ------------------------------ PUBLIC FUNCTIONS ------------------------------
--------------------------------------------------------------------------------

function		line_func(	self,
							x0,		-- 1st point x --
							y0,		-- 1st point y --
							x1,		-- 2nd point x --
							y1,		-- 2nd point y --
							func,	-- function to call --
							... )	-- additional parameters for passed func --
	local		dx, dy
	local		sx, sy
	local		err

	dx =  abs( x1 - x0 );
	sx = ( x0 < x1 ) and 1 or -1;
	dy = -abs( y1 - y0 );
	sy = ( y0 < y1 ) and 1 or -1;
	err = dx + dy;
	while true do
		func( x0, y0, ... );
		if x0 == x1 and y0 == y1 then
			return
		end
		e2 = 2 * err;
		if e2 >= dy then
			err = err + dy;
			x0 = x0 + sx;
		end
		if e2 <= dx then
			err = err + dx;
			y0 = y0 + sy;
		end
	end
end

--------------
 -- return --
--------------
return line_func

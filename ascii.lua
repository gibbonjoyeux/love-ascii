--[ GIBBON JOYEUX ]--
-- ascii.lua

----------------
-- [ MODULE ] --
----------------

local	path = ( ... ):match("(.-)[^%.]+$") .. ".src."

require ( path .. "math" )

---------------------------
 -- module ( CREATION ) --
---------------------------
local ascii =
{
	-- ATTRIBUTES --
	canvas = {},							-- ascii characters canvas --
	layer = {},								-- ascii characters layer --
	mouse_x = 0,							-- mouse x relatively to canvas --
	mouse_y = 0,							-- mouse y relatively to canvas --
	width = 0,								-- canvas width in char --
	height = 0,								-- canvas height in char --
	layer_height = 0,						-- active layer height in char --
	layer_width = 0,						-- active layer width in char --
	real_width = 0,							-- canvas width in px --
	real_height = 0,						-- canvas height in px --
	offset_x = 0,							-- canvas drawing x offset --
	offset_y = 0,							-- canvas drawing y offset --
	char_width = 0,							-- character width --
	char_height = 0,						-- character height --
	-- CONSTANTS --
	FORMAT_CHAR = 0,						-- init dimension mode --
	FORMAT_PIXEL = 1
}

----------------------------
 -- module ( FUNCTIONS ) --
----------------------------

--------------------------------------------------------------------------------
 ------------------------------ LOCAL FUNCTIONS -------------------------------
--------------------------------------------------------------------------------

local function	compute_offset( self )
	self.offset_x = floor( love.graphics.getWidth() - self.real_width ) / 2
	self.offset_y = floor( love.graphics.getHeight() - self.real_height ) / 2
end

local function	compute_mouse( self )
	local		x, y

	x, y = love.mouse.getPosition()
	self.mouse_x = round( ( x - self.offset_x ) / self.char_width )
	self.mouse_y = round( ( y - self.offset_y ) / self.char_height )
end

local function	compute_dimensions(	self,	-- ascii module --
									width,	-- width --
									height,	-- height --
									mode, 	-- dimension mode (PIXEL or CHAR) --
									font )	-- font
	--[[
	--
	-- Compute canvas and characters dimensions
	--
	--]]

	---------------------------
	-- characters dimensions --
	---------------------------
	self.char_width = font:getWidth( "X" )
	self.char_height = font:getHeight()
	-----------------------
	-- canvas dimensions --
	-----------------------
	if mode == ascii.FORMAT_CHAR then
		self.width = width
		self.height = height
	else
		self.width = floor( width / self.char_width )
		self.height = floor( height / self.char_height )
	end
	self.real_width = self.width * self.char_width
	self.real_height = self.height * self.char_height
end

--------------------------------------------------------------------------------
 ------------------------------ PUBLIC FUNCTIONS ------------------------------
--------------------------------------------------------------------------------

function		ascii:init(	width,		-- canvas width --
							height,		-- canvas height --
							mode,		-- dimension mode (PIXEL or CHAR) --
							font_path,	-- font file path --
							font_size,	-- font size --
							hinting,	-- font hinting / aliasing (see Love2d) --
							dpiscale )	-- font dpiscale (see Love2d) --
	--[[
	--
	-- Initialize ascii:
	--  . init font
	--  . create canvas
	--  . set dimensions
	--
	--]]
	local		x, y
	local		new_line
	local		font
	
	--------------
	-- set font --
	--------------
	font = love.graphics.newFont( font_path, font_size, hinting, dpiscale )
	love.graphics.setFont( font )
	compute_dimensions( self, width, height, mode, font );
	compute_offset( self )
	-------------------
	-- create canvas --
	-------------------
	self.canvas = {}
	for y = 1, self.height do
		new_line = {}
		for x = 1, self.width do
			new_line[ x ] = ' '
		end
		self.canvas[ y ] = new_line
	end
	self.layer = self.canvas
	self.layer_height = #self.layer
	self.layer_width = #self.layer[ 1 ]
end

function	ascii:update()
	compute_offset( self )
	compute_mouse( self )
end

function	ascii:clear()
	local	x, y

	for y = 1, self.layer_height do
		for x = 1, self.layer_width do
			self.layer[ y ][ x ] = ' '
		end
	end
end

function	ascii:draw(	off_x,	-- x offset to draw canvas --
						off_y )	-- y offset to draw canvas --
	local	x, y
	local	char

	-------------------
	-- handle offset --
	-------------------
	if off_x == nil then
		off_x = self.offset_x
	end
	if off_y == nil then
		off_y = self.offset_y
	end
	-----------------
	-- draw canvas --
	-----------------
	for y = 1, self.height do
		for x = 1, self.width do
			char = self.canvas[ y ][ x ]
			if char and char ~= ' ' then
				love.graphics.print(	char,
										off_x + ( x - 1 ) * self.char_width,
										off_y + ( y - 1 ) * self.char_height )
			end
		end
	end
end

-------------------------
 -- module ( RETURN ) --
-------------------------
ascii.create_layer	= require ( path .. "create_layer" )
ascii.set_layer		= require ( path .. "set_layer" )
ascii.draw_layer	= require ( path .. "draw_layer" )
ascii.line_func 	= require ( path .. "line_func" )
ascii.line			= require ( path .. "line" )
ascii.polygon		= require ( path .. "polygon" )
ascii.fill			= require ( path .. "fill" )
ascii.box			= require ( path .. "box" )
ascii.text			= require ( path .. "text" )
ascii.text_height	= require ( path .. "text_height" )

return ascii

RATIO			= 0.5
BASE_WIDTH		= 1920
BASE_HEIGHT		= 1080
WIDTH			= BASE_WIDTH * RATIO
HEIGHT			= BASE_HEIGHT * RATIO

function love.conf(t)
	-- set game info --
	t.window.width = WIDTH
	t.window.height = HEIGHT
	t.window.vsync = 1
	t.window.title = "motion"

	-- disable unused modules --
	t.modules.physics = false
	t.modules.video = false
	t.modules.touch = false
end

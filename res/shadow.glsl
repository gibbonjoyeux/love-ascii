#define PI			3.141592653589793

extern number		sigma;
extern vec2			offset;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
	float	x, y;
	float	power, weight;
	vec2	pixel_coords;
	vec4	pixel_value;
	float	total_weight;

	/// NO BLURRING
	if (sigma == 0.0) {
		return vec4( color.rgb, Texel( texture, texture_coords - offset / love_ScreenSize.xy ).a );
	}
	/// BLURRING
	color.a = 0;
	total_weight = 0.0;
	for ( y = -sigma; y <= sigma; ++y ) {
		for ( x = -sigma; x <= sigma; ++x ) {
			/// COMPUTE WEIGHT
			power = exp( - ( ( x * x + y * y ) / ( 2.0 * sigma * sigma ) ) );
			weight = ( 1.0 / ( 2.0 * PI * sigma * sigma ) ) * power;
			/// LOAD CORRESPONDING PIXEL
			pixel_coords = texture_coords + ( vec2(x, y) - offset ) / love_ScreenSize.xy;
			pixel_value = Texel( texture, pixel_coords );
			/// ADD WEIGHT
			color.a += pixel_value.a * weight;
			total_weight += weight;
		}
	}
	/// GET FINAL COLOR
	color.a = color.a / total_weight;
	return color;
}

#define PI			3.141592653589793

extern number		sigma;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
	float	x, y;
	float	power, weight;
	vec2	pixel_coords;
	vec4	pixel_value;
	vec4	value;
	float	total_weight;

	value = vec4( 0.0, 0.0, 0.0, 1.0 );
	total_weight = 0.0;
	for ( y = -sigma; y <= sigma; ++y ) {
		for ( x = -sigma; x <= sigma; ++x ) {
			power = exp( - ( ( x * x + y * y ) / ( 2.0 * sigma * sigma ) ) );
			weight = ( 1.0 / ( 2.0 * PI * sigma * sigma ) ) * power;
			pixel_coords = texture_coords + vec2(x, y) / love_ScreenSize.xy;
			pixel_value = Texel( texture, pixel_coords );
			value.rgb += pixel_value.rgb * weight;
			total_weight += weight;
		}
	}
	value.rgb = value.rgb / total_weight;
	return value;
}
